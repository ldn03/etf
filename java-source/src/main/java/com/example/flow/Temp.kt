/*
package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.*
import net.corda.core.crypto.TransactionSignature
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.OpaqueBytes
import net.corda.core.utilities.unwrap
import net.corda.finance.USD
import net.corda.finance.contracts.asset.Cash
import net.corda.finance.flows.CashIssueFlow
import java.util.*


class MyState(val partyA : Party, val partyB : Party, val someOtherField : Int) : ContractState {
    override val participants = listOf(partyA, partyB)
}

class MyContract : Contract {
    companion object {
        val CONTRACT_ID = "com.example.flow.MyContract"
    }

    interface Commands : CommandData {
        class Settle : Commands
        class Transfer : Commands
    }

    override fun verify(tx: LedgerTransaction) {

    }

}

@CordaSerializable
class ETFRequest(val amount : Int)

@CordaSerializable
class ETFResponse(val states : List<StateAndRef<MyState>>)

@InitiatingFlow
class MyInitiatingFlow(val counterparty : Party) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
        // get notary
        val notary = serviceHub.networkMapCache.notaryIdentities.single()

        // build TX
        val builder : TransactionBuilder = TransactionBuilder(notary)
        val state = MyState(serviceHub.myInfo.legalIdentities.single(),
                counterparty,
                10)
        builder.addOutputState(state, MyContract.CONTRACT_ID)
        builder.addOutputState(state, MyContract.CONTRACT_ID)
        builder.addCommand(MyContract.Commands.Settle(), state.partyB.owningKey, state.partyA.owningKey)

        // verify
        builder.verify(serviceHub)

        // sign
        val signedTx : SignedTransaction = serviceHub.signInitialTransaction(builder)

        // collect signatures
        val session = initiateFlow(counterparty)
        val allSignedTx = subFlow(CollectSignaturesFlow(signedTx, listOf(session)))

        // notarise
        val notarisedTx = subFlow(FinalityFlow(allSignedTx))

        /// -----------------------------------

        session.send("Hello")

        serviceHub.vaultService.queryBy()
QueryCriteria
        CashIssueFlow

        subFlow(CashIssueFlow(Amount(100, USD), OpaqueBytes.of(0), notary))
        val anotherBuilder = TransactionBuilder(notary)

        Cash.generateSpend(serviceHub, builder, Amount(50, USD), counterparty)



        // verify
        anotherBuilder.verify(serviceHub)

        // sign
        val signedTx2 : SignedTransaction = serviceHub.signInitialTransaction(anotherBuilder)


        val allSignedTx2 = subFlow(CollectSignaturesFlow(signedTx2, listOf(session)))

        // notarise
        val notarisedTx2 = subFlow(FinalityFlow(allSignedTx2))

        // --------------------------------------------------

        session.sendAndReceive<ETFResponse>(ETFRequest(10))
        // add states to TX builder



        return notarisedTx


    }

}


@InitiatedBy(MyInitiatingFlow::class)
class MyResponder(val session: FlowSession) : FlowLogic<Unit>() {

    @Suspendable
    override fun call() {
        val signResponder = object  : SignTransactionFlow(session) {
            override fun checkTransaction(stx: SignedTransaction) {

            }

        }
        val myVar = "Hello"
        signResponder.call()
        val anotherSignResponder = object  : SignTransactionFlow(session) {
            override fun checkTransaction(stx: SignedTransaction) {

            }

        }
        anotherSignResponder.call()

        // ------------------------------
        val payload = session.receive<ETFRequest>().unwrap { it }
        //...
        session.send(...)
    }

}
*/