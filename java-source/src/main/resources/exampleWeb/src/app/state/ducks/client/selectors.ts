import { AppState } from "../../index";
import { denormalizeEtfs } from "../market/schema";

import { ETF } from '../..'
import { Client } from './types'

export const getClient = (state: AppState): Client => {
    const client = state.client
    const etfs = client.details.etfs.map<ETF>(etfId => getEtf(state, etfId));

    return {
        ...(client.details as any),
        etfs        
    }
}

export const getAllEtfs = (state: AppState) => {
    const keys = Object.keys(state.market.details.entities.etfs)
    return keys.map(id => getEtf(state, id))
}

export const getEtf = (state: AppState, id: string): ETF => {
    const etf = state.market.details.entities.etfs[id]

    const underlyings = etf.underlyings
        .map(entityId => getUnderlying(state, entityId))

    return {
        id,
        quantity: etf.quantity,
        underlyings
    }
}

export const getUnderlying = (state: AppState, entityId: string) => {
    return state.market.details.entities.underlyings[entityId]
}
