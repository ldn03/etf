import { handleAction } from "redux-actions"

import { marketDetailsRequested, marketDetailsReceived, marketBuyReceived, marketBuyRequested } from "./actions"
import { MarketState } from "./index";

const initialState: MarketState = { loading: false, faulted: false }

const requestedMarketDetails = handleAction(marketDetailsRequested,
    (state, action) => ({
        ...state,
        loading: true
    }), initialState)

const receivedMarketDetails = handleAction(marketDetailsReceived,
    (state, action) => ({
        loading: false,
        faulted: isError(action),
        details: action.payload
    }), initialState)

const requestedBuy = handleAction(marketBuyRequested,
    (state, action) => ({
        ...state,
        loading: true
    }), initialState)

const receivedBuy = handleAction(marketBuyReceived,
    (state, action) => ({
        ...state,
        loading: false,
        faulted: isError(action),
        postReturn: action.payload
    }), initialState)

function isError(action) {
    if (action.payload instanceof Error) {
        return true
    }
    return false
}

function reduceReducers(...reducers) {
    return (previous, current) =>
      reducers.reduce(
        (p, r) => r(p, current),
        previous
      );
  }

export default reduceReducers(requestedMarketDetails, receivedMarketDetails, requestedBuy, receivedBuy)
