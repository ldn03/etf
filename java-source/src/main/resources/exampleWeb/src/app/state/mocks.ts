import { ETFs, Underlyings } from '.'

export const etfs: ETFs = [{
    id: "ETF.1",
    quantity: 2,
    underlyings: [
        {
            price: 1.5,
            quantity: 100,
            ric: "VOD"
        },
        {
            price: 1.5,
            quantity: 100,
            ric: "GOOG"
        }
    ]
},
{
    id: "ETF.2",
    quantity: 3,
    underlyings: [
        {
            price: 2,
            quantity: 200,
            ric: "WIFI"
        },
        {
            price: 3,
            quantity: 50,
            ric: "NVDA"
        },
        {
            price: 4,
            quantity: 1000,
            ric: "TTWO"
        },
        {
            price: 5,
            quantity: 70,
            ric: "ATVI"
        }
    ]
}]

export const underlyings: Underlyings = [
    {
        price: 1.5,
        quantity: 100,
        ric: "VOD"
    },
    {
        price: 1.5,
        quantity: 100,
        ric: "GOOG"
    },
    {
        price: 2,
        quantity: 200,
        ric: "WIFI"
    },
    {
        price: 3,
        quantity: 50,
        ric: "NVDA"
    },
    {
        price: 4,
        quantity: 1000,
        ric: "TTWO"
    },
    {
        price: 5,
        quantity: 70,
        ric: "ATVI"
    }
]