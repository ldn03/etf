import { normalize, schema, denormalize } from "normalizr"
import { ETFs } from "../../types";

const underlyingSchema = new schema.Entity("underlyings",
    undefined,
    {
        idAttribute: (value, parent) => `${parent.id}_${value.ric}`
    })

const etfSchema = new schema.Entity("etfs",
    { underlyings: [underlyingSchema] },
)

const obj = new schema.Object({
    etfs: [etfSchema]
})

export const normalizeMarket = (etfs) => {
    return normalize(etfs, obj)
}

export const denormalizeEtfs = (ids: string[], entities) => {
    denormalize(ids, obj, entities)
}
