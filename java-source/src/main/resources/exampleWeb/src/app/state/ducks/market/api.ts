import axios from "axios"
import { Observable } from "rxjs/Observable"

export const getMarketDetails = () => {
    return Observable.fromPromise(axios.get("/etfs"))
        .map(x => { return { etfs: x.data }})
}

export const putBuy = (id: string, quantity: number) => {
    return Observable.fromPromise(axios.put(
            `/purchase?etfTicker=${id}&etfUnits=${quantity}&etfUnitPrice=${1}&authorisedParty=C=US,L=New York,O=AuthorisedParticipant`))
        .map(x => x.data)
}

export const putCreate = (id: string, quantity: number) => {
    return Observable.fromPromise(axios.put(
            `/etf-creation?etfTicker=${id}&etfUnits=${quantity}&custodianName=C=FR,L=Paris,O=Custodian`))
        .map(x => x.data)
}

export const putRedeem = (id: string, quantity: number) => {
    return Observable.fromPromise(axios.put(
            `/etf-creation?etfTicker=${id}&etfUnits=-${quantity}&custodianName=C=FR,L=Paris,O=Custodian`))
        .map(x => x.data)
}

const getUnderlyings = () => {
    return Observable.fromPromise(axios.get("/available-underlyings"))
       .map(x => x.data)
}