import { ETFs, RemoteResource } from "../.."

export interface Market {
    readonly entities: {[key: string]: any}
}

export interface MarketState extends RemoteResource<Market>{

}

export const MARKET_REQUESTED = "MARKET/REQUESTED"
export const MARKET_RECEIVED = "MARKET/RECEIVED"

export const MARKET_UPDATED = "MARKET/UPDATED"

export const MARKET_DETAILS_REQUESTED = "MARKET/DETAILS/REQUESTED"
export const MARKET_DETAILS_RECEIVED = "MARKET/DETAILS/RECEIVED"

export const MARKET_BUY_REQUESTED = "MARKET/BUY/REQUESTED"
export const MARKET_BUY_RECEIVED = "MARKET/BUY/RECEIVED"

export const MARKET_CREATE_REQUESTED = "MARKET/CREATE/REQUESTED"
export const MARKET_CREATE_RECEIVED = "MARKET/CREATE/RECEIVED"

export const MARKET_REDEEM_REQUESTED = "MARKET/REDEEM/REQUESTED"
export const MARKET_REDEEM_RECEIVED = "MARKET/REDEEM/RECEIVED"
