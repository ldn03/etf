import { Component, OnInit } from "@angular/core"
import { Client } from "../../state/ducks/client";
import { NgRedux } from "@angular-redux/store";
import { AppState } from "../../state";
import { getClient } from "../../state/ducks/client/selectors"

@Component({
    selector: "etf-client-overview",
    template: `
<div>
    <div class="ui inverted segment overview">
        <div class="ui three mini statistics">
            <div class="ui green inverted statistic">
                <div class="value">
                    {{client.balance | currency}}
                </div>
                <div class="label">Cash</div>
            </div>
            <div class="ui orange inverted statistic">
                <div class="value">
                    {{client.etfs.length}}
                </div>
                <div class="label">ETFs owned</div>
            </div>
            <div class="ui orange inverted statistic">
                <div class="value">
                    {{numUnderlyings()}}
                </div>
                <div class="label">Underlyings</div>
            </div>
        </div>
    </div>
</div>`
})
export class ClientOverviewComponent implements OnInit {
    client: Client;

    constructor(private ngRedux: NgRedux<AppState>) {
    }

    ngOnInit(): void {
        this.ngRedux
            .select(state => getClient(state))
            .do(client => console.log("client", client))
            .do(client => this.client = client)
            .subscribe()
    }

    numUnderlyings() {
        return this.client.etfs.reduce((acc, curr) => acc + curr.underlyings.length, 0)
    }
}
