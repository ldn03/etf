import { Component, Input } from '@angular/core'

import { ETF, Underlying } from '../../state';
import { underlyings } from '../../state/mocks';

@Component({
    selector: 'etf-etf-header',
    template: `
    <span class="etf-header">
        <span>{{etf.id}}</span><span></span>
        <span class="ui label">
            Quantity:
            <span class="detail">
                {{etf.quantity}}
            </span>
        </span>
        <span class="ui label green">{{etf.underlyings.length}} underlyings</span>
        <span class="ui label">
            NAV:
            <span class="detail">
                {{getNav() | currency }}
            </span>
        </span>
    <span> </span>`
})
export class EtfHeaderComponent {
    @Input() etf: ETF

    getNav(): number {
        const notionals = this.etf.underlyings.map(underlying => this.getNotional(underlying))

        return notionals.reduce((total, num) => total + num)
    }

    getNotional(underlying: Underlying): number {
        return underlying.price * underlying.quantity
    }
}