import { Component, OnInit } from "@angular/core"
import { NgRedux } from "@angular-redux/store"
import { Observable } from "rxjs/Observable"

import { AppState, ETFs } from "../../state"
import { Market } from "../../state/ducks/market"
import { operations } from "../../state/ducks/market"
import { getAllEtfs, getClient } from "../../state/ducks/client/selectors";

@Component({
    selector: "etf-client-market",
    template: `
        <etf-etfs *ngIf="etfs" [etfs]="etfs" [buttons]="functions[role]"></etf-etfs>`
})
export class ClientMarketComponent implements OnInit {
    etfs: ETFs
    role: string

    constructor(private ngRedux: NgRedux<AppState>) {
    }

    ngOnInit(): void {
        this.ngRedux
            .select(state => getAllEtfs(state))
            .do(x => this.etfs = x)
            .do(x => this.ngRedux.dispatch(operations.updateMarket([{
                id: "1",
                quantity: 1,
                underlyings: [{
                    ric: "VOD",
                    price: 100,
                    quantity: 100
                }]
            }])))
            .subscribe()

        this.ngRedux
            .select(state => getClient(state))
            .do(x => this.role = x.role)
            .subscribe()
    }

    onBuyClick = (event: Event, id: string, quantity: number) => {
        event.stopPropagation();
        this.ngRedux.dispatch(operations.requestBuy(id, quantity))
    }

    onCreateClick = (event: Event, id: string, quantity: number) => {
        event.stopPropagation();
        this.ngRedux.dispatch(operations.requestCreate(id, quantity))
    }

    onRedeemClick = (event: Event, id: string, quantity: number) => {
        event.stopPropagation();
        this.ngRedux.dispatch(operations.requestRedeem(id, quantity))
    }

    readonly functions = {
        'Client': [
            { text: 'Buy', onClick: this.onBuyClick }
        ],
        'Authorised Participant': [
            { text: 'Create', onClick: this.onCreateClick },
            { text: 'Redeem', onClick: this.onRedeemClick }
        ],
        'Custodian': []
    }
}
