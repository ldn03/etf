import { Component, OnInit} from "@angular/core"
import { NgRedux } from "@angular-redux/store"
import { Observable } from "rxjs/Observable"

import { AppState } from "../../state"
import { Client } from "../../state/ducks/client"
import { getClient } from "../../state/ducks/client/selectors";

@Component({
    selector: "etf-client-holdings",
    template: `
    <etf-etfs *ngIf="client" [etfs]="client.etfs" [buttons]='[]'></etf-etfs>`
})
export class ClientHoldingsComponent implements OnInit {
    client: Client;
    
    constructor(private ngRedux: NgRedux<AppState>) { }

    ngOnInit(): void {
        this.ngRedux
        .select(state => getClient(state))
        .do(x => this.client = x)
        .subscribe()
    }
}
