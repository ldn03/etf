import { Component, ChangeDetectorRef, OnInit } from "@angular/core"
import { NgRedux } from "@angular-redux/store"
import { NavigationEnd, ActivatedRoute, UrlSegment, Router } from "@angular/router"

import { AppState } from "../../state"
import { Client } from "../../state/ducks/client";
import { getClient } from "../../state/ducks/client/selectors"

@Component({
    selector: "etf-client",
    template: `
    <div class="page {{getClass()}}">
        <div class="sidemenu">
            <etf-client-menu 
                [links]="links"
                [logo]="getLogo()">
            </etf-client-menu>
        </div>
        <div class="main-content">
            <div class="ui inverted navbar">
                <div class="main ui container">
                    <div class="ui basic right aligned segment">
                        <span class="ui small header">{{client.location}}</span>
                        &nbsp;
                        <i class="{{client.country}} flag"></i>
                    </div>
                </div>
            </div>
            <div class="main ui container">
            <div class="ui vertical segment">
                <h1 class="ui huge header">{{activeLink?.title}}</h1>
            </div>
            <div class="ui vertical segment">
                <router-outlet></router-outlet>
            </div>
        </div>
    <div>`
})
export class ClientPageComponent {
    
    links: NavLink[]

    private currentSection: string
    activeLink: NavLink
    private client: Client

    constructor(private route: ActivatedRoute,
        private router: Router,
        private changeRef: ChangeDetectorRef,
        private ngRedux: NgRedux<AppState>) {
        this.links = [
            { path: "overview", displayName: "Overview", title: "Overview" },
            { path: "holdings", displayName: "Holdings", title: "Holdings" },
            { path: "market", displayName: "Market", title: "Market" }
        ]
    }

    ngOnInit(): void {        
        const path = this.route.firstChild && this.route.firstChild.snapshot.url[0].path
        this.currentSection = `/user/${path}`  
        
        this.activeLink = this.getActiveLink()
        
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe(event => {
                this.currentSection = (event as any).url
                this.activeLink = this.getActiveLink()
            })
        
        this.ngRedux.select(x => getClient(x))
            .subscribe(client => this.client = client)
            console.log(this.client)
    }

    getActiveLink() {
        return this.links.filter(x =>  `/user/${x.path}` === this.currentSection)[0]
    }

    getLogo() {
        const role = this.client.role
        switch(role)
        {
            case "Client":
                return "fidelity.png"
            case "Authorised Participant":
                return "cs.png"
            case "Custodian":
                return "blackrock.png"
        }
    }

    getClass() {
        const role = this.client.role
        switch(role)
        {
            case "Client":
                return "client"
            case "Authorised Participant":
                return "ap"
            case "Custodian":
                return "custodian"
        }
    }
}

interface NavLink {
    readonly path: string
    readonly displayName: string
    readonly title?: string
}
