package com.example.contract

import com.example.state.IOUState
import com.example.state.PurchaseState
import com.example.state.UnderlyingState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

open class BuyUnderlyingContract : Contract {
    companion object {
        @JvmStatic
        val CONTRACT_ID = "com.example.contract.BuyUnderlyingContract"
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands.Create>()
        requireThat {
            "The underlyings should only be sent to a single party" using (tx.outputs.size == 1)
            val out = tx.outputsOfType<UnderlyingState>().single()
            "The buyer and seller cannot be the same entity." using (out.buyer != out.seller)
            "All of the participants must be signers." using (command.signers.containsAll(out.participants.map { it.owningKey }))

            "The requested unit count must be greater than 0." using (out.units > 0)
        }
    }

    /**
     * This contract only implements one command, Create.
     */
    interface Commands : CommandData {
        class Create : Commands
    }
}
