package com.example.schema

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

object UnderlyingSchema

object UnderlyingSchemaV1 : MappedSchema(
        schemaFamily = UnderlyingSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PersistentUnderlying::class.java)) {
    @Entity
    @Table(name = "underlying_states")
    class PersistentUnderlying(
            @Column(name = "buyer")
            var buyer: String,

            @Column(name = "seller")
            var seller: String,

            @Column(name = "ticker")
            var ticker: String,

            @Column(name = "units")
            var units: Double,

            @Column(name = "purchaseDate")
            var purchaseDate: Date,

            @Column(name = "purchasePrice")
            var purchasePrice: Double,

            @Column(name = "linear_id")
            var linearId: UUID
    ) : PersistentState()
}