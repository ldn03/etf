package com.example.state

import com.example.schema.PurchaseSchemaV1
import com.example.schema.UnderlyingSchemaV1
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState

import java.util.*

data class UnderlyingState (
                    val ticker: String,
                    val units: Double,
                    val purchaseDate: Date,
                    val purchasePrice: Double,
                    val buyer: Party,
                    val seller: Party,
                    override val linearId: UniqueIdentifier = UniqueIdentifier()):
        LinearState, QueryableState {

    override val participants: List<AbstractParty> get() = listOf(buyer, seller)

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is UnderlyingSchemaV1 -> UnderlyingSchemaV1.PersistentUnderlying(
                    this.buyer.name.toString(),
                    this.seller.name.toString(),
                    this.ticker,
                    this.units,
                    this.purchaseDate,
                    this.purchasePrice,
                    this.linearId.id
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(UnderlyingSchemaV1)
}
