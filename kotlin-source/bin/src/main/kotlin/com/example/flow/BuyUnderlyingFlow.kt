package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import com.example.contract.PurchaseContract
import com.example.contract.PurchaseContract.Companion.PURCHASE_CONTRACT_ID
import com.example.models.UnderlyingRequest
import com.example.state.PurchaseState
import com.example.state.UnderlyingState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.util.*

object BuyUnderlyingFlow {
    @InitiatingFlow
    @StartableByRPC
    class BuyUnderlyingInitiator(
            private var underlyingRequest: UnderlyingRequest,
            private val seller: Party
    ) : FlowLogic<SignedTransaction>() {

        @Suspendable
        override fun call(): SignedTransaction {
            // Obtain a reference to the notary we want to use.
            val notary = serviceHub.networkMapCache.notaryIdentities[0]

            // Stage 1.
            // Generate an unsigned transaction.
            val underlyingState = UnderlyingState(
                    underlyingRequest.ticker,
                    underlyingRequest.units,
                    Date(),
                    underlyingRequest.purchasePrice,
                    serviceHub.myInfo.legalIdentities.first(),
                    seller
            )

            val txCommand = Command(PurchaseContract.Commands.Create(), underlyingState.participants.map { it.owningKey })
            val txBuilder = TransactionBuilder(notary).withItems(StateAndContract(underlyingState, PURCHASE_CONTRACT_ID), txCommand)

            // Stage 2.
            // Verify that the transaction is valid.
            txBuilder.verify(serviceHub)

            // Stage 3.
            // Sign the transaction.
            val partSignedTx = serviceHub.signInitialTransaction(txBuilder)

            // Stage 4.
            // Send the state to the seller, and receive it back with their signature.
            val otherPartyFlow = initiateFlow(seller);
            val fullySignedTx = subFlow(CollectSignaturesFlow(partSignedTx, setOf(otherPartyFlow)))

            // Stage 5.
            // Notarise and record the transaction in both parties' vaults.
            return subFlow(FinalityFlow(fullySignedTx))
        }
    }

    @InitiatedBy(BuyUnderlyingInitiator::class)
    class Acceptor(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    val output = stx.tx.outputs.single().data
                    "This must be an Underlying transaction." using (output is UnderlyingState)

                    // Check My Balance
                            serviceHub.vaultService.queryBy<UnderlyingState>()
                            .vaultQueryBy<IOUState>().states
                }
            }

            return subFlow(signTransactionFlow)
        }
    }
}
