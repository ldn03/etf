package com.example.models

import net.corda.core.serialization.CordaSerializable
import java.util.*

@CordaSerializable
class UnderlyingRequest constructor(
        val ticker: String,
        val units: Double,
        val purchasePrice: Double,
        val purchaseDate: Date
) {

}