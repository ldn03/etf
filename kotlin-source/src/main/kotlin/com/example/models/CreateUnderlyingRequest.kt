
package com.example.models

import net.corda.core.serialization.CordaSerializable


@CordaSerializable
class CreateUnderlyingRequest constructor(val underlyingTicker: String, val quantity: Double) {

}