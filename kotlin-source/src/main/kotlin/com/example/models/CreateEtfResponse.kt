package com.example.models

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
class CreateETFResponse constructor(val etfTicker: String, val etfUnits: Double, val etfUnitPrice: Double) {

}
