package com.example.models

import net.corda.core.serialization.CordaSerializable


@CordaSerializable
class CreateEtfRequest constructor(val etfTicker: String, val etfUnits: Double, val etfUnitPrice: Double) {

}