package com.example.contract

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

open class UnderlyingContract : Contract {
    companion object {
        @JvmStatic
        val CONTRACT_ID = "com.example.contract.UnderlyingContract"
    }

    override fun verify(tx: LedgerTransaction) {

    }

    /**
     * This contract only implements one command, Create.
     */
    interface Commands : CommandData {
        class Issue : TypeOnlyCommandData(), Commands
        class Transfer : TypeOnlyCommandData(), Commands
    }
}
