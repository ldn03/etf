package com.example.contract

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

open class EtfContract : Contract {
    companion object {
        @JvmStatic
        val ETF_CONTRACT_ID = "com.example.contract.EtfContract"
    }

    override fun verify(tx: LedgerTransaction) {


    }

    /**
     * This contract only implements one command, Create.
     */
    interface Commands : CommandData {
        class Issue : TypeOnlyCommandData(), Commands
        class Transfer : TypeOnlyCommandData(),Commands
    }
}
