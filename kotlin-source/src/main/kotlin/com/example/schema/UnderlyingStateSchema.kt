package com.example.schema

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

/**
 * The family of schemas for EtfState.
 */
object UnderlyingStateSchema

/**
 * An EtfState schema.
 */
object UnderlyingStateSchemaV1 : MappedSchema(
        schemaFamily = EtfStateSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PersistentUnderlying::class.java)) {
    @Entity
    @Table(name = "underlying_states")
    class PersistentUnderlying(
            @Column(name = "Ticker")
            var etfTicker: String,

            @Column(name = "owner")
            var owner: String,

            @Column(name = "quantity")
            var quantity: Double,

            @Column(name = "linear_id")
            var linearId: UUID
    ) : PersistentState()
}