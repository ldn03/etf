package com.example.schema

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

/**
 * The family of schemas for EtfState.
 */
object EtfStateSchema

/**
 * An EtfState schema.
 */
object EtfStateSchemaV1 : MappedSchema(
        schemaFamily = EtfStateSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PersistentEtf::class.java)) {
    @Entity
    @Table(name = "etf_states")
    class PersistentEtf(
            @Column(name = "etfTicker")
            var etfTicker: String,

            @Column(name = "owner")
            var owner: String,

            @Column(name = "previousOwner")
            var previousOwner: String,

            @Column(name = "quantity")
            var quantity: Double,

            @Column(name = "linear_id")
            var linearId: UUID
    ) : PersistentState()
}