package com.example.market

import com.example.api.ExampleApi
import com.example.flow.PurchaseFlow
import com.example.models.PurchaseRequest
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.utilities.getOrThrow
import java.util.*
import javax.ws.rs.core.Response

object MarketSimulator {

    var applicationInitDate: Date = Date();
    var unitsOnMarket: Int = 0;

    fun start() {
        applicationInitDate = Date();
    }

    fun getPrices(): MarketPrices {
        var t = getTValue()

        var navPrice = t

        var marketPrice = navPrice + (t * (t / 1000)) * unitsOnMarket;
        if (marketPrice < 0) {
            marketPrice = 0.0;
        }

        var underlying1Price = marketPrice * 0.6
        var underlying2Price = marketPrice * 0.4

        var shouldPanic = (marketPrice - navPrice) > 1000

        return MarketPrices(marketPrice, navPrice, underlying1Price, underlying2Price, shouldPanic)
    }

    fun getUnits(): Int {
        return unitsOnMarket
    }

    fun getTValue(): Double {
        var currentDate = Date()
        return (currentDate.time - applicationInitDate.time).toDouble() / 1000
    }

    fun placeOrder(units: Int): Int {
        unitsOnMarket += units
        return units
    }

    fun createToLowerPrice() {

        //
    }
}