package com.example.market

class MarketPrices constructor(
        val market: Double,
        val nav: Double,
        val underlying1: Double,
        val underlying2: Double,
        var shouldPanic: Boolean
)