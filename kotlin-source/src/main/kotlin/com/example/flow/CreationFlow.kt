
package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import com.example.contract.EtfContract
import com.example.contract.UnderlyingContract
import com.example.models.CreateEtfRequest
import com.example.models.CreateUnderlyingRequest
import com.example.state.EtfState
import com.example.state.UnderlyingState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.unwrap


object CreationFlow {
    @InitiatingFlow
    @StartableByRPC
    class CreationFlowInitiator(
            val etfTicker: String,
            var etfUnits: Double,
            val custodian: Party) : FlowLogic<SignedTransaction>() {

        @Suspendable
        fun selfIssueUnderlying(request : CreateUnderlyingRequest) : UniqueIdentifier {
            val us = serviceHub.myInfo.legalIdentities.single()
            val underlyingState = UnderlyingState(request.underlyingTicker, us, request.quantity)
            val notary = serviceHub.networkMapCache.notaryIdentities.single()
            val builder = TransactionBuilder(notary)
                    .addOutputState(underlyingState, UnderlyingContract.CONTRACT_ID)
                    .addCommand(UnderlyingContract.Commands.Issue(), us.owningKey)
            builder.verify(serviceHub)
            val selfSignedIssuance = serviceHub.signInitialTransaction(builder)
            subFlow(FinalityFlow(selfSignedIssuance))
            return underlyingState.linearId
        }

        @Suspendable
        override fun call(): SignedTransaction {
            // Obtain a reference to the notary we want to use.
            val notary = serviceHub.networkMapCache.notaryIdentities[0]

            // Generate an unsigned transaction.

            val commodity1 ="FB"
            val commodity2 = "TWTR"
            val commodity1Amount = 6 * etfUnits
            val commodity2Amount = 4 * etfUnits
            val underlying1Id = selfIssueUnderlying(CreateUnderlyingRequest(commodity1, commodity1Amount) )
            val underlying2Id = selfIssueUnderlying(CreateUnderlyingRequest(commodity2, commodity2Amount) )


            val underlying1State = serviceHub.vaultService.queryBy(UnderlyingState::class.java,
                    QueryCriteria.LinearStateQueryCriteria(linearId = listOf(underlying1Id))).states.single()
            val underlying2State = serviceHub.vaultService.queryBy(UnderlyingState::class.java,
                    QueryCriteria.LinearStateQueryCriteria(linearId = listOf(underlying2Id))).states.single()


            val us = serviceHub.myInfo.legalIdentities.single()
            val txBuilder = TransactionBuilder(notary)

            txBuilder.addInputState(underlying1State)
                    .addInputState(underlying2State)
                    .addOutputState(underlying1State.state.data.wihNewOwner(custodian), UnderlyingContract.CONTRACT_ID)
                    .addOutputState(underlying2State.state.data.wihNewOwner(custodian), UnderlyingContract.CONTRACT_ID)
                    .addCommand(UnderlyingContract.Commands.Transfer(), us.owningKey, custodian.owningKey)

            //todo calculate etf price based on NAV
            val etfPrice= 100.0
            val flowSession = initiateFlow(custodian)
            //transfer etf
            val counterpartyInputState = flowSession.sendAndReceive<StateAndRef<EtfState>>(CreateEtfRequest(etfTicker, etfUnits, etfPrice)).unwrap { it }


            txBuilder.addInputState(counterpartyInputState)
                    .addOutputState(counterpartyInputState.state.data.wihNewOwner(us), EtfContract.ETF_CONTRACT_ID)
                    .addCommand(EtfContract.Commands.Transfer(), us.owningKey, custodian.owningKey)

            subFlow(ReceiveStateAndRefFlow<EtfState>(flowSession))
            subFlow(SendStateAndRefFlow(flowSession, listOf(underlying1State, underlying2State)))

            // Stage 2.
            // Verify that the transaction is valid.
            txBuilder.verify(serviceHub)

            // Stage 3.
            // Sign the transaction.
            val partSignedTx = serviceHub.signInitialTransaction(txBuilder)

            // Stage 4.

            // Send the state to the counterparty, and receive it back with their signature.
            val fullySignedTx = subFlow(CollectSignaturesFlow(partSignedTx, setOf(flowSession)))

            // Stage 5.
            // Notarise and record the transaction in both parties' vaults.

            var notorisedTx =  subFlow(FinalityFlow(fullySignedTx))


            return notorisedTx
        }
    }

    @InitiatedBy(CreationFlowInitiator::class)
    class Acceptor(val flowSession: FlowSession) : FlowLogic<SignedTransaction>() {

        @Suspendable
        fun selfIssueEtf(request : CreateEtfRequest) : UniqueIdentifier {
            val us = serviceHub.myInfo.legalIdentities.single()
            val etfState = EtfState(request.etfTicker, us, us,
                    request.etfUnits)
            val notary = serviceHub.networkMapCache.notaryIdentities.single()
            val builder = TransactionBuilder(notary)
                    .addOutputState(etfState, EtfContract.ETF_CONTRACT_ID)
                    .addCommand(EtfContract.Commands.Issue(), us.owningKey)
            builder.verify(serviceHub)
            val selfSignedIssuance = serviceHub.signInitialTransaction(builder)
            subFlow(FinalityFlow(selfSignedIssuance))
            return etfState.linearId
        }

        @Suspendable
        override fun call(): SignedTransaction {
            val request = flowSession.receive<CreateEtfRequest>().unwrap { it }
            val issuedStateId = selfIssueEtf(request)
            val inputEtfState = serviceHub.vaultService.queryBy(EtfState::class.java,
                    QueryCriteria.LinearStateQueryCriteria(linearId = listOf(issuedStateId))).states.single()

            flowSession.send(inputEtfState)

            subFlow(SendStateAndRefFlow(flowSession, listOf(inputEtfState)))
            subFlow(ReceiveStateAndRefFlow<UnderlyingState>(flowSession))


            val signTransactionFlow = object : SignTransactionFlow(flowSession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                     stx.tx.outputs.forEach{output->
                         "This must be an underlying transaction." using (output.data is UnderlyingState || output.data is EtfState)

                     }
                }
            }

            return subFlow(signTransactionFlow)
        }
    }
}

