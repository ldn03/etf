package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.Amount
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.utilities.OpaqueBytes
import net.corda.finance.USD
import net.corda.finance.flows.AbstractCashFlow
import net.corda.finance.flows.CashIssueFlow

object IssueCashFlow {
    @InitiatingFlow
    @StartableByRPC
    class IssueCashInitiator(val amount: Long) : FlowLogic<AbstractCashFlow.Result >() {

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): AbstractCashFlow.Result {
            // Obtain a reference to the notary we want to use.
            val notary = serviceHub.networkMapCache.notaryIdentities[0]

            // Self issue cash amount
            val cashAmount = Amount(amount * 100, USD)
            return subFlow(CashIssueFlow(cashAmount, OpaqueBytes.of(0), notary))
        }
    }
}
