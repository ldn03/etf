package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import com.example.contract.EtfContract
import com.example.contract.EtfContract.Companion.ETF_CONTRACT_ID
import com.example.models.PurchaseRequest
import com.example.state.EtfState
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.finance.USD
import net.corda.finance.flows.CashPaymentFlow

object PurchaseFlow {
    @InitiatingFlow
    @StartableByRPC
    class PurchaseInitiator(var purchaseRequest: PurchaseRequest,
                    val authorisedParty: Party) : FlowLogic<SignedTransaction>() {

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): SignedTransaction {
            // Obtain a reference to the notary we want to use.
            val notary = serviceHub.networkMapCache.notaryIdentities[0]

            // Determine cash of etf price
            val totalCashMovement = purchaseRequest.etfUnitPrice * purchaseRequest.etfUnits

            // Self issue cash amount
            val cashAmount = Amount(Math.ceil(totalCashMovement).toLong() * 100, USD)

            val session = initiateFlow(authorisedParty)

            // Determine movement of ETF inventory
            val etfState = EtfState(purchaseRequest.etfTicker, serviceHub.myInfo.legalIdentities.first(), authorisedParty, purchaseRequest.etfUnits)

            // Stage 1.
            // Generate an unsigned transaction.
            val txCommand = Command(EtfContract.Commands.Transfer(), etfState.participants.map { it.owningKey })
            val txBuilder = TransactionBuilder(notary).withItems(StateAndContract(etfState, ETF_CONTRACT_ID), txCommand)

            // Stage 2.
            // Verify that the transaction is valid.
            txBuilder.verify(serviceHub)

            // Stage 3.
            // Sign the transaction.
            val partSignedTx : SignedTransaction = serviceHub.signInitialTransaction(txBuilder)

            // Stage 4.
            // Send the state to the counterparty, and receive it back with their signature.
            val fullySignedTx = subFlow(CollectSignaturesFlow(partSignedTx, setOf(session)))

            // Stage 5.
            // Notarise and record the transaction in both parties' vaults.
            subFlow(FinalityFlow(fullySignedTx))


            return subFlow(CashPaymentFlow(cashAmount, authorisedParty, anonymous = false)).stx
        }
    }

    @InitiatedBy(PurchaseInitiator::class)
    class Acceptor(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    val output = stx.tx.outputsOfType<EtfState>().single()
                    "This must be an EtfState transaction." using (output is EtfState)
                }
            }

            return subFlow(signTransactionFlow)
        }
    }
}
