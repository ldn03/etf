package com.example.api

import com.example.flow.CreationFlow
import com.example.flow.IssueCashFlow
import com.example.flow.PurchaseFlow.PurchaseInitiator
import com.example.market.MarketSimulator
import com.example.models.PurchaseRequest
import com.example.state.EtfState
import com.example.state.UnderlyingState
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor
import net.corda.finance.contracts.asset.Cash
import org.slf4j.Logger
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.core.Response.Status.CREATED

val SERVICE_NAMES = listOf("Controller", "Network Map Service")

@Path("/")
class ExampleApi(private val rpcOps: CordaRPCOps) {
    private val myLegalName: CordaX500Name = rpcOps.nodeInfo().legalIdentities.first().name

    companion object {
        private val logger: Logger = loggerFor<ExampleApi>()
    }

    @GET
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    fun whoami() = mapOf("me" to myLegalName)

    @GET
    @Path("peers")
    @Produces(MediaType.APPLICATION_JSON)
    fun getPeers(): Map<String, List<CordaX500Name>> {
        val nodeInfo = rpcOps.networkMapSnapshot()
        return mapOf("peers" to nodeInfo
                .map { it.legalIdentities.first().name }
                .filter { it.organisation !in (SERVICE_NAMES + myLegalName.organisation) })
    }

    @GET
    @Path("cash/all")
    @Produces(MediaType.APPLICATION_JSON)
    fun getAllCashStates() = rpcOps.vaultQueryBy<Cash.State>().states

    @GET
    @Path("cash/latest")
    @Produces(MediaType.APPLICATION_JSON)
    fun getLatestCashStates() = rpcOps.vaultQueryBy<Cash.State>().states.last()

    @PUT
    @Path("cash/issue")
    fun creation(
            @QueryParam("amount") amount: Long) : Response {

        return try {
            val flowHandle = rpcOps.startTrackedFlow(IssueCashFlow::IssueCashInitiator, amount)

            // The line below blocks and waits for the future to resolve.
            val result = flowHandle.returnValue.getOrThrow()
            Response.status(CREATED).entity("Transaction ${result} committed to ledger.\n").build()

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            Response.status(BAD_REQUEST).entity(ex.message!!).build()
        }
    }


    @GET
    @Path("etfs/all")
    @Produces(MediaType.APPLICATION_JSON)
    fun getAllEtfStates() = rpcOps.vaultQueryBy<EtfState>().states

    @GET
    @Path("etfs/latest")
    @Produces(MediaType.APPLICATION_JSON)
    fun getLatestEtfStates() = rpcOps.vaultQueryBy<EtfState>().states.last()

      @GET
    @Path("underlyings/all")
    @Produces(MediaType.APPLICATION_JSON)
    fun getAllUnderlyings() = rpcOps.vaultQueryBy<UnderlyingState>().states

    @GET
    @Path("underlyings")
    @Produces(MediaType.APPLICATION_JSON)
    fun getUnderlyings(@QueryParam("ticker") etfTicker: String) = rpcOps.vaultQueryBy<UnderlyingState>().states.find { state -> state.state.data.underlyingTicker == etfTicker }

    @PUT
    @Path("etf-creation")
    fun creation(
            @QueryParam("etfTicker") etfTicker: String,
            @QueryParam("etfUnits") etfUnits: Double,
            @QueryParam("custodianName") custodianName: CordaX500Name?) : Response {
        if (etfTicker.isNullOrEmpty()) {
            return Response.status(BAD_REQUEST).entity("Query parameter 'etfTicker' must not be null or empty.\n").build()
        }
        if (custodianName == null) {
            return Response.status(BAD_REQUEST).entity("Query parameter 'custodianName' missing or has wrong format.\n").build()
        }
        val custodian = rpcOps.wellKnownPartyFromX500Name(custodianName) ?:
                return Response.status(BAD_REQUEST).entity("Custodian named $custodianName cannot be found.\n").build()

        return try {
            val flowHandle = rpcOps.startTrackedFlow(CreationFlow::CreationFlowInitiator, etfTicker, etfUnits, custodian)
            flowHandle.progress.subscribe { println(">> $it") }

            // The line below blocks and waits for the future to resolve.
            val result = flowHandle.returnValue.getOrThrow()

            Response.status(CREATED).entity("Transaction id ${result.id} committed to ledger.\n").build()

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            Response.status(BAD_REQUEST).entity(ex.message!!).build()
        }
    }

    @PUT
    @Path("purchase")
    fun createPurchase(
            @QueryParam("etfTicker") etfTicker: String,
            @QueryParam("etfUnits") etfUnits: Double,
            @QueryParam("etfUnitPrice") etfUnitPrice: Double,
            @QueryParam("authorisedParty") authorisedPartyName: CordaX500Name?): Response {

        if (etfTicker.isEmpty()) {
            return Response.status(BAD_REQUEST).entity("Query parameter 'etfTicker' missing or has wrong format.\n").build()
        }
        if (etfUnits <= 0 ) {
            return Response.status(BAD_REQUEST).entity("Query parameter 'etfUnits' must be non-negative.\n").build()
        }
        if (authorisedPartyName == null) {
            return Response.status(BAD_REQUEST).entity("Query parameter 'authorisedParty' missing or has wrong format.\n").build()
        }

        val authorisedParty = rpcOps.wellKnownPartyFromX500Name(authorisedPartyName) ?:
                return Response.status(BAD_REQUEST).entity("Party named $authorisedPartyName cannot be found.\n").build()

        val purchaseRequest = PurchaseRequest(etfTicker, etfUnits, etfUnitPrice);

        return try {
            val flowHandle = rpcOps.startTrackedFlow(::PurchaseInitiator, purchaseRequest, authorisedParty)
            flowHandle.progress.subscribe { println(">> $it") }

            // The line below blocks and waits for the future to resolve.
            val result = flowHandle.returnValue.getOrThrow()

            Response.status(CREATED).entity("Transaction id ${result.id} committed to ledger.\n").build()

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            Response.status(BAD_REQUEST).entity(ex.message!!).build()
        }
    }

    @GET
    @Path("market/prices")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMarketPrices() = MarketSimulator.getPrices()

    @GET
    @Path("market/start")
    @Produces(MediaType.APPLICATION_JSON)
    fun start() = MarketSimulator.start()

    @GET
    @Path("market/time")
    @Produces(MediaType.APPLICATION_JSON)
    fun getTime() = MarketSimulator.getTValue()

    @GET
    @Path("market/units")
    @Produces(MediaType.APPLICATION_JSON)
    fun getUnits() = MarketSimulator.getUnits()

    @POST
    @Path("market/order")
    @Produces(MediaType.APPLICATION_JSON)
    fun placeOrder(@QueryParam("units") units: String) = MarketSimulator.placeOrder(units.toInt())
}