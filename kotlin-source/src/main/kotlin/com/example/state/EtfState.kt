package com.example.state

import com.example.schema.EtfStateSchemaV1
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable

/**
 * The state object recording ETF movements between participants.
 *
 * A state must implement [ContractState] or one of its descendants.
 *
 * @param etfTicker the ETF ticker.
 * @param owner the party who owns the ETF.
 * @param previousOwner the party receiving the ETF.
 * @param quantity number of etfs being moved.
 */
@CordaSerializable
data class EtfState(val etfTicker: String,
                    val owner: Party,
                    val previousOwner: Party,
                    val quantity: Double,
                    override val linearId: UniqueIdentifier = UniqueIdentifier()):
        LinearState, QueryableState {
    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = listOf(owner, previousOwner)

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is EtfStateSchemaV1 -> EtfStateSchemaV1.PersistentEtf(
                    this.etfTicker,
                    this.owner.name.toString(),
                    this.previousOwner.name.toString(),
                    this.quantity,
                    this.linearId.id
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(EtfStateSchemaV1)

    fun wihNewOwner(newOwner : Party) = copy(owner = newOwner, previousOwner = owner)
}
