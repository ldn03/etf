package com.example.state

import com.example.schema.UnderlyingStateSchemaV1
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState

data class UnderlyingState(val underlyingTicker: String,
                           val owner: Party,
                           val quantity: Double,
                           override val linearId: UniqueIdentifier = UniqueIdentifier()):
LinearState, QueryableState {

    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = listOf(owner)

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is UnderlyingStateSchemaV1 -> UnderlyingStateSchemaV1.PersistentUnderlying(
                    this.underlyingTicker,
                    this.owner.name.toString(),
                    this.quantity,
                    this.linearId.id
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(UnderlyingStateSchemaV1)


    fun wihNewOwner(newOwner : Party) = copy(owner = newOwner)
}
