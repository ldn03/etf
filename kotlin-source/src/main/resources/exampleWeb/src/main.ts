import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { configureMock as configureClientMock } from "./app/state/ducks/client/mock"
import { configureMock as configureMarketMock } from "./app/state/ducks/market/mock"

import axios from "axios"
import * as MockAdapter from "axios-mock-adapter"

axios.defaults.baseURL = '/api'

const mock = new MockAdapter(axios)

declare let require: any;

configureClientMock(mock)
configureMarketMock(mock)

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
