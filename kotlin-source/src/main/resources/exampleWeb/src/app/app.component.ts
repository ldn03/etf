import { Component } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { AppState } from './state';

import { operations as clientOperations } from "./state/ducks/client"
import { operations as marketOperations } from "./state/ducks/market"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'app';

  constructor(private ngRedux: NgRedux<AppState>) {
    this.ngRedux.dispatch(clientOperations.requestClient())
    this.ngRedux.dispatch(marketOperations.requestMarketDetails())

    setTimeout(() =>
      this.ngRedux.dispatch(marketOperations.updateMarket({
        id: "ETF.1",
        quantity: 1,
        dummy: true,
        marketValue: 127,
        nav: 123,
        underlyings: [{
          ric: "VOD",
          price: 100,
          quantity: 100
        }, {
          ric: "GOOG",
          price: 125.98,
          quantity: 100
        }]
      })
    ), 1000)

    setTimeout(() =>
    this.ngRedux.dispatch(marketOperations.updateMarket({
      id: "ETF.1",
      quantity: 1,
      dummy: true,
      marketValue: 127,
      nav: 123,
      underlyings: [{
        ric: "VOD",
        price: 100,
        quantity: 100
      }, {
        ric: "GOOG",
        price: 125.98,
        quantity: 100
      }]
    })
  ), 1000)

  setTimeout(() =>
  this.ngRedux.dispatch(marketOperations.updateMarket({
    id: "ETF.1",
    quantity: 1,
    dummy: true,
    marketValue: 127,
    nav: 123,
    underlyings: [{
      ric: "VOD",
      price: 100,
      quantity: 100
    }, {
      ric: "GOOG",
      price: 125.98,
      quantity: 100
    }]
  })
), 1000)

setTimeout(() =>
this.ngRedux.dispatch(marketOperations.updateMarket({
  id: "ETF.1",
  quantity: 1,
  dummy: true,
  marketValue: 127,
  nav: 123,
  underlyings: [{
    ric: "VOD",
    price: 100,
    quantity: 100
  }, {
    ric: "GOOG",
    price: 125.98,
    quantity: 100
  }]
})
), 1000)

setTimeout(() =>
this.ngRedux.dispatch(marketOperations.updateMarket({
  id: "ETF.1",
  quantity: 1,
  dummy: true,
  marketValue: 127,
  nav: 123,
  underlyings: [{
    ric: "VOD",
    price: 100,
    quantity: 100
  }, {
    ric: "GOOG",
    price: 125.98,
    quantity: 100
  }]
})
), 1000)

      
  }
}
