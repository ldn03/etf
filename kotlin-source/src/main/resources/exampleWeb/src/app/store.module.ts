import { NgModule } from '@angular/core';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';

import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import { default as rootReducer, AppState } from './state';

import { environment } from '../environments/environment';


@NgModule({
    imports: [NgReduxModule]
})
export class StoreModule {
    constructor(
        private ngRedux: NgRedux<AppState>,
        private devTools: DevToolsExtension) {

        let enhancers = [];
        // ... add whatever other enhancers you want.

        const initialState: AppState = {
            client: {
                faulted: false,
                loading: false,
                details: {
                    balance: 0,
                    etfs: []
                }
            },
            market: {
                faulted: false,
                loading: false,
                details: {
                    entities: {}
                }
            }
        };

        const middleware = [thunk, createLogger()]

        // You probably only want to expose this tool in devMode.
        if (!environment.production && devTools.isEnabled()) {
            enhancers = [...enhancers, devTools.enhancer()];
        }

        ngRedux.configureStore(
            rootReducer as any,
            initialState,
            middleware,
            enhancers);
    }
}
