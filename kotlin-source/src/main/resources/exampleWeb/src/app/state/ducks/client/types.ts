import { ETFs, RemoteResource } from "../.."

export interface Client {
    readonly balance: number
    readonly etfs: ETFs,
    readonly role: string
    readonly country: string
    readonly location: string
}

export interface NormalizedClient {
    readonly balance: number
    readonly etfs: string[]
}

export interface ClientState extends RemoteResource<NormalizedClient>{ }

export const CLIENT_DETAILS_REQUESTED = "CLIENT/DETAILS/REQUESTED"
export const CLIENT_DETAILS_RECEIVED = "CLIENT/DETAILS/RECEIVED"
