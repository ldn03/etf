import MockAdapter from "axios-mock-adapter"

import { etfs } from '../../mocks'

export const configureMock = (mock) => {
    /*mock.onGet("/me")
        .reply(200, {
            // me: "C=FR,L=Paris,O=Custodian"
            // me: "C=US,L=New York,O=AuthorisedParticipant"

            // me: "C=GB,L=London,O=Client"
            me: "C=GB,L=London,O=Client2"
        })
=======
            me: "C=GB,L=London,O=Client"
        })*/

    mock.onGet('/me').passThrough();


    mock.onGet("/cash/latest")
        .reply(200, 1000000)

    mock.onGet("etf/latest")
        .reply(200, ["ETF.1", "ETF.2"])
}