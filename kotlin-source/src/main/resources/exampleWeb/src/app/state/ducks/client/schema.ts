import { normalize, schema } from "normalizr"
import { Client } from "./types"

export const normalizeClient = (client: Client) => {
    const underlyingSchema = new schema.Entity("underlyings",
        undefined,
        {
            idAttribute: (value) => value.ric
        })

    const etfSchema = new schema.Entity("etfs",
        { underlyings: [underlyingSchema] },
        { idAttribute: value => value.id }
    )

    const clientSchema = new schema.Object({
        efts: [etfSchema]
    })

    return normalize(client, clientSchema)
}