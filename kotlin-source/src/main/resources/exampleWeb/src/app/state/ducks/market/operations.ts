
import {
    marketDetailsReceived,
    marketDetailsRequested,
    marketBuyReceived,
    marketBuyRequested,
    marketCreateReceived,
    marketCreateRequested,
    marketRedeemReceived,
    marketRedeemRequested,
    marketUpdated
} from "./actions"

import { ThunkAction } from "redux-thunk"

import * as toastr from 'toastr'

import { getMarketDetails, putBuy, putCreate, putRedeem } from "./api"
import { dispatch } from "@angular-redux/store";
import { ETFs, ETF } from "../../types";

import { normalizeMarket } from "./schema"

function requestMarketDetails(): ThunkAction<any, any, any> {
    return (dispatch, getState) => {
        dispatch(marketDetailsRequested())

        getMarketDetails()
            .catch(x => dispatch(marketDetailsReceived(x)))
            .subscribe(x => {
                const normalized = normalizeMarket((x as any))
                dispatch(marketDetailsReceived(normalized))
            })
    }
}

function requestBuy(id: string, quantity: number): ThunkAction<any, any, any> {
    return (dispatch, getState) => {
        dispatch(marketBuyRequested())

        putBuy(id, quantity)
            .catch(x => dispatch(marketBuyReceived(x)))
            .subscribe(x => {
                toastr.success(`Bought ${quantity} ${id}`)
                return dispatch(marketBuyReceived(x))
            })
    }
}

function requestCreate(id: string, quantity: number): ThunkAction<any, any, any> {
    return (dispatch, getState) => {
        dispatch(marketCreateRequested())

        putCreate(id, quantity)
            .catch(x => dispatch(marketCreateReceived(x)))
            .subscribe(x => {
                toastr.success(`Created ${quantity} ${id}`)
                return dispatch(marketCreateReceived(x))
            })
    }
}

function requestRedeem(id: string, quantity: number): ThunkAction<any, any, any> {
    return (dispatch, getState) => {
        dispatch(marketRedeemRequested())

        putRedeem(id, quantity)
            .catch(x => dispatch(marketRedeemReceived(x)))
            .subscribe(x => {
                toastr.success(`Redeemed ${quantity} ${id}`)
                return dispatch(marketRedeemReceived(x))
            })
    }
}

function updateMarket(etf: ETF) {
    return marketUpdated(etf)
}

export default {
    updateMarket,
    requestMarketDetails,
    requestBuy,
    requestCreate,
    requestRedeem
}