import MockAdapter from "axios-mock-adapter"

import { etfs, underlyings } from '../../mocks'

export const configureMock = (mock) => {
    mock.onGet("etfs")
        .reply(200, etfs)

    mock.onGet("available-underlyings")
        .reply(200, underlyings)

    mock.onPut(/purchase\?.*/)
        .reply(200)
    
    mock.onPut(/etf-creation\?.*/)
        .reply(200)
}