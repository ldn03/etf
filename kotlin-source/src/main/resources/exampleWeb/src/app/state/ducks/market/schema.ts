import { normalize, schema, denormalize } from "normalizr"
import { ETFs } from "../../types";
import { ETF } from "../../index";



export const normalizeEtf = (etf) => {
    const underlyingSchema = new schema.Entity("underlyings",
        undefined,
        {
            idAttribute: (value, parent) => `${parent.id}_${value.ric}`
        })

    const etfSchema = new schema.Entity("etfs",
        { underlyings: [underlyingSchema] },
    )

    const obj = new schema.Object({
        etfs: [etfSchema]
    })

    return normalize(etf, obj)
}

export const normalizeMarket = (etfs) => {
    const underlyingSchema = new schema.Entity("underlyings",
        undefined,
        {
            idAttribute: (value, parent) => `${parent.id}_${value.ric}`,
            processStrategy: (value) => [value]
        })

    const etfSchema = new schema.Entity("etfs",
        { underlyings: [underlyingSchema] }
    )

    const obj = new schema.Object({
        etfs: [etfSchema]
    })
    const normalized =  normalize(etfs, obj)

    return {
        ...normalized,
        entities:  {
            ...normalized.entities,
            etfs: processEtfs(normalized.entities.etfs)
        }
    }
}

function processEtfs(etfs) {
    const newEtfs = {...etfs}
    console.log("etfs", etfs)
    console.log("newEtfs", newEtfs)
    Object.keys(etfs).forEach(key => newEtfs[key] = ([{...etfs[key]}]))

    return newEtfs;
}