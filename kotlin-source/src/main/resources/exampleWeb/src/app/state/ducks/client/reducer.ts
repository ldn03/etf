import { handleAction } from "redux-actions"

import { clientRequested, clientReceived } from "./actions"
import { ClientState } from "./types"

const initialState: ClientState = { loading: false, faulted: false }

const requestedClient = handleAction(clientRequested,
    (state, action) => ({
        ...state,
        loading: true,
    }), initialState)

const receivedClient = handleAction(clientReceived,
    (state, action) => ({
        loading: false,
        faulted: isError(action),
        details: action.payload
    }), initialState)

function isError(action) {
    if (action.payload instanceof Error) {
        return true
    }
    return false
}

function reduceReducers(...reducers) {
    return (previous, current) =>
      reducers.reduce(
        (p, r) => r(p, current),
        previous
      );
  }

export default reduceReducers(requestedClient, receivedClient)
