import { ClientState } from "./ducks/client"
import { MarketState } from "./ducks/market"

export interface AppState {
    client: ClientState
    market: MarketState
}

export interface Underlying {
    readonly ric: string
    readonly price: number
    readonly quantity: number
}

export interface ETF {
    readonly id: string
    readonly nav: number,
    readonly marketValue: number,
    readonly dummy: boolean,
    readonly quantity: number
    readonly underlyings: Underlyings
}

export type ETFs = ReadonlyArray<ETF>

export type Underlyings = ReadonlyArray<Underlying>

export interface RemoteResource<TData> {
    readonly faulted: boolean
    readonly loading: boolean
    readonly details?: TData
}