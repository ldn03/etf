import { createAction } from "redux-actions"
import {
    MARKET_DETAILS_RECEIVED,
    MARKET_DETAILS_REQUESTED,
    MARKET_BUY_RECEIVED,
    MARKET_BUY_REQUESTED,
    MARKET_CREATE_RECEIVED,
    MARKET_CREATE_REQUESTED,
    MARKET_REDEEM_RECEIVED,
    MARKET_REDEEM_REQUESTED,
    MARKET_UPDATED
} from "./types"

export const marketDetailsRequested = createAction(MARKET_DETAILS_REQUESTED)
export const marketDetailsReceived = createAction(MARKET_DETAILS_RECEIVED)

export const marketBuyRequested = createAction(MARKET_BUY_REQUESTED)
export const marketBuyReceived = createAction(MARKET_BUY_RECEIVED)

export const marketCreateRequested = createAction(MARKET_CREATE_REQUESTED)
export const marketCreateReceived = createAction(MARKET_CREATE_RECEIVED)

export const marketRedeemRequested = createAction(MARKET_REDEEM_REQUESTED)
export const marketRedeemReceived = createAction(MARKET_REDEEM_RECEIVED)

export const marketUpdated = createAction(MARKET_UPDATED)
