import { handleAction } from "redux-actions"

import { marketDetailsRequested, marketDetailsReceived, marketBuyReceived, marketBuyRequested, marketUpdated } from "./actions"
import { MarketState } from "./index";
import { ETF } from "../../types";
import { normalizeMarket, normalizeEtf } from "./schema";
import { AppState } from "../../index";

const initialState: MarketState = { loading: false, faulted: false }

const requestedMarketDetails = handleAction(marketDetailsRequested,
    (state, action) => ({
        ...state,
        loading: true
    }), initialState)

const receivedMarketDetails = handleAction(marketDetailsReceived,
    (state, action) => ({
        loading: false,
        faulted: isError(action),
        details: action.payload
    }), initialState)

const requestedBuy = handleAction(marketBuyRequested,
    (state, action) => ({
        ...state,
        loading: true
    }), initialState)

const receivedBuy = handleAction(marketBuyReceived,
    (state, action) => ({
        ...state,
        loading: false,
        faulted: isError(action),
        postReturn: action.payload
    }), initialState)


const updatedMarket = handleAction(marketUpdated,
    (state, action) => {
        return updateUnderlyings(action.payload, state)
    }, initialState)


function updateUnderlyings(etf: ETF, state: MarketState) {
    const normalized = normalizeEtf({ etfs: [etf] })
    const normalizedEtf = Object.keys(normalized.entities.etfs).map(key => normalized.entities.etfs[key]).filter((x, idx) => idx ===0)[0]
    const normalizedUnderlyings = normalized.entities.underlyings

    console.log("normalized", normalized)
    console.log("underlyings", normalizedUnderlyings)

    const stateUnderlyings = state.details.entities.underlyings
    console.log("stateUnderlyings", stateUnderlyings)

    const newUnderlyings = Object.keys(stateUnderlyings)
        .map(x => {console.log(x); return x})
        .map(underlyingId => normalizedUnderlyings[underlyingId] == undefined
            ? {key: underlyingId, underlying: stateUnderlyings[underlyingId] }
            : updateUnderlying(stateUnderlyings[underlyingId], normalizedUnderlyings[underlyingId], etf))

    const underlyingsMap = {}
    newUnderlyings.forEach(x => {
        underlyingsMap[x.key] = x.underlying
    })
    
    console.log("newUnderlyings", newUnderlyings, underlyingsMap)
    console.log("normalizedEtf", normalizedEtf)

    return {
        ...state,
        details: {
            ...state.details,
            entities: {
                etfs: {
                    ...state.details.entities.etfs,
                    [normalizedEtf.id]: updateEtfs(state.details.entities.etfs[normalizedEtf.id], normalizedEtf)
                },
                underlyings: underlyingsMap
            }
        }
    }
}
function updateEtfs(currentEtfsArray: any[], etf: any) {
    if(currentEtfsArray.length > 30) {
        return [
            ...currentEtfsArray.filter((x, idx) => idx !== 0),
            etf
        ]
    } else {
        return [
            ...currentEtfsArray,
            etf
        ]
    }
}

function updateUnderlying(currentUnderlyingArray: any[], newUnderlying, etf: ETF) {
    const key = `${etf.id}_${newUnderlying.ric}`
    if(currentUnderlyingArray.length > 30)
    {
        return {key, underlying: [
            ...currentUnderlyingArray.filter((x, idx) => idx !== 0),
            newUnderlying
        ]}
    } else{
        return {key, underlying: [
            ...currentUnderlyingArray,
            newUnderlying
        ]
    }
    }
}


function isError(action) {
    if (action.payload instanceof Error) {
        return true
    }
    return false
}


function reduceReducers(...reducers) {
    return (previous, current) =>
        reducers.reduce(
            (p, r) => r(p, current),
            previous
        );
}

export default reduceReducers(requestedMarketDetails, receivedMarketDetails, requestedBuy, receivedBuy, updatedMarket)
