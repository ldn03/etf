import { clientReceived, clientRequested } from "./actions"

import { ThunkAction } from "redux-thunk"

import { normalizeClient } from "./schema"

import { getClientDetails } from "./api"
import { dispatch } from "@angular-redux/store";

function requestClient() : ThunkAction<any, any, any> {
    return (dispatch, getState) => {
        dispatch(clientRequested())

        getClientDetails()
            // .map(x => normalizeClient(x))
            .catch(x => dispatch(clientReceived(x)))
            .subscribe(x => dispatch(clientReceived(x)))
    }
}

export default {
    requestClient
}