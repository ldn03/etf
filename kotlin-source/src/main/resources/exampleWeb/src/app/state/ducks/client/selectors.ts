import { AppState } from "../../index";

import { ETF } from '../..'
import { Client } from './types'

export const getClient = (state: AppState): Client => {
    const client = state.client
    const etfs = client.details.etfs.map<ETF>(etfId => getEtf(state, etfId));

    return {
        ...(client.details as any),
        etfs
    }
}

export const getAllEtfs = (state: AppState) => {
    console.log("getAllEtfs", state)
    const keys = Object.keys(state.market.details.entities.etfs)
    const allEtfs =  keys.map(id => getEtf(state, id))

    console.log("allEtfs", allEtfs)
    return allEtfs
}

export const getEtfHistory = (state: AppState, id) => {
    console.log("getEtfHistory", state, id)
    return state.market.details.entities.etfs[id] as any[]
}

export const getEtf = (state: AppState, id: string): ETF => {
    console.log("getEtf", state, id)
    const etfData = state.market.details.entities.etfs[id] as any[]
    const lastEtf = etfData[etfData.length - 1]

    const underlyings = lastEtf.underlyings
        .map(entityId => getUnderlying(state, entityId))

    return {
        ...lastEtf,
        underlyings
    }
}


export const getUnderlying = (state: AppState, entityId: string) => {
    console.log("getUnderlying", state, entityId)
    const underlyingsData = state.market.details.entities.underlyings[entityId] as any[]
    const latestUnderlying = underlyingsData[underlyingsData.length - 1]

    return latestUnderlying
}
