import { combineReducers } from "redux"

import { default as client } from "./ducks/client"
import { default as market } from "./ducks/market"

export default combineReducers({client, market})