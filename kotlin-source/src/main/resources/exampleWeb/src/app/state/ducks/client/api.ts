import axios from "axios"
import { Observable } from "rxjs/Observable"

export const getClientDetails = () => {
    return Observable.combineLatest(
        getClientBalance(),
        getClientEtfs(),
        getClientInfo(),
        (balance, etfs, info) => {
            return {
                balance,
                etfs,
                ...info
            }
        }
    )
}

const getClientInfo = () => {
    return Observable.fromPromise(axios.get("/me"))
        .map(x => x.data.me)
        .map(str => {
            const split = str.split(",")
            const props = split.map(part => part.split("="))
            return props
        }).map(props => ({
            country: props[0][1].toLowerCase(),
            location: props[1][1],
            role: getDisplayRole(props[2][1])
        }))
}

const getDisplayRole = (role: string) => {
    if (role === 'AuthorisedParticipant') {
        return 'Authorised Participant'
    }

    return role
}

const getClientBalance = () => {
    return Observable.fromPromise(axios.get("/cash/latest"))
        .map(x => x.data)
}

const getClientEtfs = () => {
    return Observable.fromPromise(axios.get("/etf/latest"))
        .map(x => x.data)
}