import { createAction } from "redux-actions"

import { CLIENT_DETAILS_RECEIVED, CLIENT_DETAILS_REQUESTED } from "./types"

export const clientRequested = createAction(CLIENT_DETAILS_REQUESTED)
export const clientReceived = createAction(CLIENT_DETAILS_RECEIVED)