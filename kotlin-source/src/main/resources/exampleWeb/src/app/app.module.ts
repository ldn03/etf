import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { SuiModule } from "ng2-semantic-ui"

import { StoreModule } from "./store.module"
import { ClientModule } from "./views/client/module"
import { AppComponent } from './app.component'
import { NavbarComponent } from "./views/layout/navbar"

const routes: Routes = [{
  path: "*", component: AppComponent
}]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    StoreModule,
    ClientModule,
    SuiModule,
    RouterModule.forRoot(routes, { enableTracing: false, useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
