import { Component, OnInit, ChangeDetectorRef, Input } from "@angular/core"
import { NavigationEnd, ActivatedRoute, UrlSegment, Router } from "@angular/router"

@Component({
    selector: "etf-client-menu",
    template: `
    <div>
        <div class="ui vertical inverted menu left">
            <div class="item company">
                <img src="assets/{{logo}}"/>
                <!-- <span class="ui inverted huge header">{{role}}</span> -->
            </div>
            <ng-container *ngFor="let link of links">
                <a class="item" [routerLink]="link.path" [ngClass]="{'active': isActive(link)}">{{link.displayName}}</a>
            </ng-container>
        </div>
    <div>`,
    styles: [".menu { border-radius: 0px } "]
})
export class ClientPageMenuComponent implements OnInit {
    private currentSection: string
    
    @Input() links
    @Input() logo: string = ''

    constructor(private route: ActivatedRoute,
        private router: Router,
        private changeRef: ChangeDetectorRef) {
    }

    ngOnInit(): void {        
        const path = this.route.firstChild && this.route.firstChild.snapshot.url[0].path
        this.currentSection = `/user/${path}`       
        
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe(event => {
                this.currentSection = (event as any).url
            })
    }

    isActive(link) {
        return `/user/${link.path}` === this.currentSection
    }
}

