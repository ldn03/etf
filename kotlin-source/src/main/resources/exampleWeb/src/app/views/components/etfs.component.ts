import { Component, Input } from '@angular/core'

import { ETF } from '../../state';

@Component({
    selector: 'etf-etfs',
    template: `
      <sui-accordion class="styled fluid" [closeOthers]="false">
            <sui-accordion-panel *ngFor="let etf of _etfs">
                <div title>
                    <i class="dropdown icon"></i>
                    <etf-etf-header [etf]="etf"></etf-etf-header>
                    <div class="ui action input" *ngIf="buttons?.length > 0">
                        <input #unitInput type="number" value="0" min="0" (click)="onInputClick($event)">
                        <button *ngFor="let button of buttons" class="ui teal button" [ngClass]="{'disabled': unitInput.value === '0'}" (click)="button.onClick($event, etf.id, unitInput.value)">
                            {{button.text}}
                        </button>
                    </div>
                </div>
                <div content class="underlyings">
                    <etf-underlyings [underlyings]="etf.underlyings"></etf-underlyings>
                    <etf-chart [etf]="etf"></etf-chart>
                </div>
            </sui-accordion-panel>
        </sui-accordion>`
})
export class EtfsComponent {
    _etfs
    @Input() 
    set etfs(value: ReadonlyArray<ETF>) {
        if(value)
        {
            this._etfs = value
        } else{
            this._etfs = []
        }
    }
    @Input() buttons: [{text: string, onClick: (event: Event, id: string, value: number) => void}]

    constructor() {
        this._etfs = []
    }
    onInputClick(event: Event): void {
        event.stopPropagation()
    }
}