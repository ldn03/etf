import { Component } from "@angular/core"

@Component({
  selector: "etf-navbar",
  template: `
    <div class="ui inverted vertical masthead center aligned segment">
    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="active item">Home</a>
        <div class="right item">
          <a class="ui inverted button">Log in</a>
          <a class="ui inverted button">Sign Up</a>
        </div>
      </div>
    </div>
  </div>`
})
export class NavbarComponent { }