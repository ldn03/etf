import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SuiAccordionModule } from "ng2-semantic-ui";

import { BalanceComponent } from "./balance.component"
import { EtfsComponent } from "./etfs.component";
import { UnderlyingsComponent } from "./underlyings.component"
import { EtfHeaderComponent } from "./etf.header"
import {EtfChartComponent} from "./chart"

import { ChartModule } from 'angular2-highcharts';

const components = [
    BalanceComponent,
    EtfsComponent,
    UnderlyingsComponent,
    EtfHeaderComponent,
    EtfChartComponent
]

@NgModule({
    imports: [
        CommonModule,
        SuiAccordionModule, 
        ChartModule.forRoot(require("highcharts"))],
    declarations: components,
    exports: components
})
export class ComponentModule {}