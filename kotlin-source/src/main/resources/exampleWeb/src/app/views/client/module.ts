import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from "@angular/common"

import { ComponentModule } from "../components/components.module"

import { ClientHoldingsComponent } from "./holdings"
import { ClientMarketComponent } from "./market"
import { ClientPageComponent } from "./page"
import { ClientPageMenuComponent } from "./menu"
import { ClientOverviewComponent } from "./overview"

const routes: Routes = [
    {
        path: "user", component: ClientPageComponent,
        children: [
            { path: "overview", component: ClientOverviewComponent },
            { path: "holdings", component: ClientHoldingsComponent },
            { path: "market", component: ClientMarketComponent }
        ]
    },
]

@NgModule({
    imports: [
        CommonModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        ClientHoldingsComponent,
        ClientMarketComponent,
        ClientPageComponent,
        ClientPageMenuComponent,
        ClientOverviewComponent]
})
export class ClientModule { }