import { Component, Input } from "@angular/core"

@Component({
    selector: 'etf-balance',
    template: `
        <div class="ui green label">
            Balance
            <div class="detail">{{balance | currency}}</div>
        </div>
    `
})
export class BalanceComponent {
    @Input() balance: number
}