import { Component, Input } from "@angular/core"

import { Underlying } from "../../state";

@Component({
    selector: 'etf-underlyings',
    template: `
<div>
    <table class="ui celled compact selectable table">
    <tr *ngFor="let underlying of underlyings">
        <td class="ui aligned header quantity">{{underlying.quantity}}</td>
        <td>
            <div class="ui first header">{{underlying.ric}}</div>
            <div class="subheader ui label">Notional {{notional(underlying) | currency}}</div>
        </td>
        <td class="right aligned">{{underlying.price | currency}}</td>
    <tr>
    </table>
</div>`
})
export class UnderlyingsComponent {
    @Input() underlyings: ReadonlyArray<Underlying>;

    notional(underlying : Underlying) {
        return underlying.price * underlying.quantity
    }
}