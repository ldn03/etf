import { Component, Input, OnInit } from '@angular/core'

import { ETF, AppState } from '../../state';
import { NgRedux } from '@angular-redux/store';
import { getEtf, getEtfHistory } from '../../state/ducks/client/selectors';


@Component({
    selector: 'etf-chart',
    template: `
<chart [options]="chartOptions" (load)="saveInstance($event.context)"></chart>
`
})
export class EtfChartComponent implements OnInit {
    @Input() etf: ETF

    chartOptions 
    chart: any

    constructor(private ngRedux: NgRedux<AppState>) { }

    ngOnInit(): void {
        this.chartOptions = {
            chart: {
                type: 'line'
            }, 
            title: '',
            series: [
                { name: "Market Value", data: [1, 2, 3] },
                { name: "Nav", data: [1, 2, 3]}
            ]
        }

        this.ngRedux.select(x => getEtf(x, this.etf.id))
            .subscribe(x =>{
                console.log("update", x.marketValue)
                console.log("marketValue", x.marketValue)
                // const marketValue = x.map(x => x.marketValue)
                // console.log("marketValue", marketValue)
                if(this.chart){
                    this.chart.series[0].addPoint(x.marketValue)
                    this.chart.redraw()
                }
                
                // this.chart.options.series[1].data.push(x.nav)
        } )
    }

    saveInstance(chartInstance): void {
        console.log("saveInstance", chartInstance)
        this.chart = chartInstance
    }
}

